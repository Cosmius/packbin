{-# LANGUAGE CPP             #-}
{-# LANGUAGE RecordWildCards #-}
module Main where

import           Options.Applicative
import           System.Directory

#if defined(darwin_HOST_OS)
import           Main.MacOS          (osMain)
#else
#error unsupported platform, maybe later?
#endif

data Args = Args { srcPath :: FilePath
                 , dstPath :: FilePath
                 }

argsParser :: Parser Args
argsParser = pure Args
  <*> strArgument (metavar "SRC")
  <*> strArgument (metavar "DST")

main :: IO ()
main = do
  let opts = info (helper <*> argsParser)
              (fullDesc <> progDesc "Package binaries")
  Args{..} <- execParser opts
  putStrLn $ "Create directory if missing: " ++ dstPath
  createDirectoryIfMissing True dstPath
  osMain srcPath dstPath
