{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE FlexibleContexts #-}
module Main.MacOS (osMain) where

import           Control.Monad
import           Control.Monad.State
import           Data.Foldable
import           Data.List                 as L
import           Data.Map                  as M
import           Data.Set                  as S
import           System.Directory
import           System.FilePath
import           System.Process

data ResolveState = ResolveState { processed :: Set String
                                 , destDir   :: FilePath
                                 } deriving Show
data BinaryInfo = BinaryInfo { rPaths :: [FilePath]
                             , dyLibs :: [String]
                             } deriving Show

osMain :: FilePath -> FilePath -> IO ()
osMain src dest = void $ runStateT (resolveBin src) ResolveState{processed=S.empty,destDir=dest}

resolveBin :: (MonadIO m, MonadState ResolveState m) => FilePath -> m ()
resolveBin fp = do
  s@ResolveState{..} <- get
  let destPath = destDir </> fileName
      fileName = takeFileName fp
  unless (fileName `S.member` processed) $ do
    put s{processed=S.insert fileName processed}
    liftIO $ do
      putStrLn $ "Processing: " ++ fp
      putStrLn $ "Copying " ++ fp ++ " to " ++ destPath
      copyFile fp destPath
    loadCommands <- liftIO $ getLoadCommands fp
    let rpaths = getRPath loadCommands
        dylibs = getDyLib loadCommands
        fixedRpaths = fixLoaderPath fp <$>  rpaths
    liftIO $ do
      callProcess "chmod" ["755", destPath]
      for_ rpaths $ \p -> callProcess "install_name_tool" ["-delete_rpath", p, destPath]
      for_ dylibs $ \p -> callProcess "install_name_tool" ["-change", p, "@rpath/" ++ takeFileName p, destPath]
      callProcess "install_name_tool" ["-add_rpath", "@loader_path", destPath]
      callProcess "install_name_tool" ["-add_rpath", "@loader_path/../lib", destPath]
      callProcess "install_name_tool" ["-add_rpath", "@loader_path/../lib64", destPath]
    for_ dylibs $ \libPath -> case libPath of
      '@':_ | "@rpath" `isPrefixOf` libPath -> do
              let reletivePath = drop (length "@rpath/") libPath
              liftIO (findDyLibByRPath fixedRpaths reletivePath) >>=
                  \r -> case r of
                Just p -> resolveBin p
                Nothing -> fail $ "Cannot find " ++ libPath ++ " from " ++ show rpaths
            | "@loader_path" `isPrefixOf` libPath ->
                resolveBin $ fixLoaderPath fp libPath
            | otherwise -> fail "NotImplemented"
      _ -> resolveBin libPath

findDyLibByRPath :: [FilePath] -> String -> IO (Maybe FilePath)
findDyLibByRPath [] name = return Nothing
findDyLibByRPath (p:ps) name = do
  let path = p </> name
  r <- doesFileExist path
  if r then return $ Just path else findDyLibByRPath ps name

type CommandKey = String
type CommandValue = String
type LoadCommand = Map CommandKey CommandValue

getLoadCommands :: FilePath -> IO [LoadCommand]
getLoadCommands fp = parseOtool <$> readProcess "otool" ["-l", fp] ""

getCommandValues :: String -> CommandKey -> [LoadCommand] -> [CommandValue]
getCommandValues command key =
  L.map (takeWhile (/=' ') . findWithDefault "" key) . L.filter ((==command) . findWithDefault "" "cmd")

fixLoaderPath :: FilePath -> FilePath -> FilePath
fixLoaderPath origin target
  | "@loader_path" `isPrefixOf` target = takeDirectory origin ++ drop (length "@loader_path") target
  | otherwise = target

getDyLib :: [LoadCommand] -> [FilePath]
getDyLib = getCommandValues "LC_LOAD_DYLIB" "name"

getRPath :: [LoadCommand] -> [FilePath]
getRPath = getCommandValues "LC_RPATH" "path"

parseOtool :: String -> [LoadCommand]
parseOtool = parseOtoolLines . lines

parseOtoolLines :: [String] -> [LoadCommand]
parseOtoolLines [] = []
parseOtoolLines xs = command : parseOtoolLines (dropToLoadCommand remains)
  where
    command = M.fromList $ L.map parseOtoolLine section
    (section, remains) = span (isPrefixOf " ") $ tail $ dropToLoadCommand xs
    dropToLoadCommand = dropWhile $ not . isPrefixOf "Load command"

parseOtoolLine :: String -> (CommandKey, CommandValue)
parseOtoolLine = fmap (dropWhile (==' ')) . break (==' ') . dropWhile (==' ')
